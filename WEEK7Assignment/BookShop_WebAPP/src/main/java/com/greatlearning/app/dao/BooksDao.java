package com.greatlearning.app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.app.bean.Books;
import com.greatlearning.app.resource.DbResource;

public class BooksDao {
	
	public List<Books> getAllBooks(){
		List<Books> listOfBooks = new ArrayList<Books>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select * from books");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Books books = new Books();
				books.setBookId(rs.getInt(1));
				books.setTitle(rs.getString(2));
				books.setGenre(rs.getString(3));						
				listOfBooks.add(books);
			}
		}catch(Exception e){
		System.out.println(e);
		}
		return listOfBooks;
	}


}
