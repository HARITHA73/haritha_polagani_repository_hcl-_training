package com.greatlearning.app.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.app.bean.Books;
import com.greatlearning.app.service.BooksService;
import com.greatlearning.app.service.LoginService;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
Books book=new Books();
		
		BooksService bs=new BooksService();
		List<Books> res=bs.getAllBooks();
		HttpSession hs=request.getSession();
		hs.setAttribute("obj",res);
	    RequestDispatcher req = request.getRequestDispatcher("displayBooks.jsp");
		req.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");

		LoginService service= new LoginService();	
		HttpSession Session = request.getSession();
		RequestDispatcher rd= request.getRequestDispatcher("login.jsp");
		if(request.getParameter("mobilenumber")!= null) {
		long mobileNumber= Long.parseLong(request.getParameter("mobileNumber"));
		String pass= request.getParameter("pass");
		String loginResult = service.verifyPassword(mobileNumber, pass);
		rd.include(request, response);
		}
	    doGet(request, response);
	}

}
