package com.greatLearning.miniProject.bean;


public class Menu {
	private int itemID;
	private String itemName;
	private int itemQuantity;
	private double itemPrice;
	
	public Menu(int itemID, String itemName, int itemQuantity, double itemPrice) throws IllegalArgumentException
	{
		super();
		
		if(itemID<0)
		{
			throw new IllegalArgumentException("exception occured, ID cannot less than zero");
		}
		this.itemID = itemID;
		
		if(itemName==null) {
			throw new IllegalArgumentException("exception occured, name cannot be null");
		}
		this.itemName = itemName;
		
		if(itemQuantity<0)
		{
			throw new IllegalArgumentException("exception occured, quantity cannot less than zero");
		}
		this.itemQuantity = itemQuantity;
		
		if(itemPrice<0)
		{
			throw new IllegalArgumentException("exception occured, price cannot less than zero");
		}
		this.itemPrice = itemPrice;
	}


	public int getItemID() {
		return itemID;
	}


	public void setItemID(int itemID) {
		this.itemID = itemID;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public int getItemQuantity() {
		return itemQuantity;
	}


	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}


	public double getItemPrice() {
		return itemPrice;
	}


	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "" + itemID + " " + itemName + " " + itemQuantity + " " + itemPrice;
	}
}
