package com.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bean.AddtoFavourites;



public interface FavouriteRepository  extends CrudRepository<AddtoFavourites, Integer>{
	List<AddtoFavourites> findAll();
}
