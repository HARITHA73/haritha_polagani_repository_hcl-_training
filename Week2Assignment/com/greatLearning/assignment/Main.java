package com.greatLearning.assignment;

import java.util.ArrayList;
public class Main {
       public static void main(String[] args) {

        ArrayList<Employee> list = new ArrayList<>();
//                  System.out.println("enter how many employee address u need to add");
        Employee e1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
                list.add(e1);

        Employee e2 = new Employee(2, "Bobby", 22, 500000, "Hr", "Bombay");

                list.add(e2);

        Employee e3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");

                list.add(e3);

        Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");

                list.add(e4);

        Employee e5 = new Employee(5, "Smitha", 24, 1200000, "Hr", "Bengaluru");

                list.add(e5);


        System.out.println(list);

        System.out.println("\n");

        DataStructureB b = new DataStructureB();

                b.monthlySalary(list);

                System.out.println("\n");
     
                b.cityNameCount(list);

                System.out.println("\n");

        DataStructureA a = new DataStructureA();

                a.sortingNames(list);

        }

}
