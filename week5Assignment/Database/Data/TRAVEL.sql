
                                                                      /*  TRAVEL ON MIND */

create database travel_haritha;

use travel_haritha;

create table PASSENGER
 (
  Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20)
);


create table PRICE
(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);

select * from price;

            /*QUIERIES*/

  /*1.*/

 select count(case when Gender='M' then 1 end) as Male,count(case when Gender='F' then 1 end) as Female from travel_haritha.passenger where Distance>=600;


  /*2.*/

select MIN(price) from travel_haritha.price where Bus_Type='Sleeper';


  /*3.*/

select Passenger_name from passenger where Passenger_name like 'S%';

 select *from travel_haritha.passenger where Passenger_name like 'S%';


  /*4.*/

 select p1.Passenger_name,p1.Boarding_city,p1.Destination_city,p1.Bus_TYpe,p2.Price from travel_haritha.passenger p1,travel_haritha.price p2 where p1.Distance=p2.Distance and p1.Bus_type=p2.Bus_type;


 /*5.*/

 select p1.Passenger_name,p1.Boarding_city,p1.Destination_city,p1.Bus_TYpe,p2.Price from travel_haritha.passenger p1,travel_haritha.price p2 where p1.Distance=1000 and p1.Bus_type="sitting";



 /* 6.*/

 select  p1.Passenger_name, p1.Destination_city as Boarding_city,p1.Boarding_city as Destination_city, p2.Bus_Type, p2.Price from travel_haritha.passenger p1,travel_haritha.price p2 where Passenger_name="Pallavi" and p1.Distance=p2.Distance;

 
 /*7.*/
select distinct (distance) from passenger order by Distance desc;


  /*8.*/

select passenger_name,distance,distance*100/t.d as "percentage" from passenger cross join (select sum(Distance) as d from passenger)t;

  /*9.*/

 create view haritha_view as select *from passenger where Category='AC';select * from haritha_view;

 


 /*11.*/

select *from travel_haritha.passenger limit 5;



