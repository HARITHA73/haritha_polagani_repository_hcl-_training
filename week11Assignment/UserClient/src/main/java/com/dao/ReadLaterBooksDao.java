package com.dao;



import org.springframework.data.jpa.repository.JpaRepository;

import com.bean.CompositeKey;
import com.bean.ReadLaterBooks;

public interface ReadLaterBooksDao extends JpaRepository<ReadLaterBooks, CompositeKey>{

}
