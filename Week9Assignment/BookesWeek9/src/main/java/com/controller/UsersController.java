package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Users;
import com.service.UsersService;

@RestController
@RequestMapping(value="user")
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@PostMapping(value="store",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody Users user) {
		return usersService.storeUserDetails(user);
		
	}
	
	@PatchMapping(value="updatePassword")
	public String updateUserPassword(@RequestBody Users user) {
		return usersService.updateUserDetails(user);
	}
	
	@GetMapping(value="AllDetails")
	public List<Users> getAllDetailsOfUsers(){
		return usersService.getAllUsersDetails();
	}
	
	@DeleteMapping(value="deleteByMail/{email}")
	public String deleteUserDetails(@PathVariable("email") String email) {
		return usersService.deleteUserDetails(email);
	}
	
}