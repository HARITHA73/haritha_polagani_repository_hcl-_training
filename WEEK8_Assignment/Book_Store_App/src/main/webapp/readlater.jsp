<%@page import="com.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h4 align="center">ReadLater BookList </h4>
<a href="Welcome.jsp">Home Page</a>
<body style="background-color:azure ;">
<div align="center">
<table border="1">
	<tr>
			<th>Id</th>
			<th>TITLE</th>
			<th>GENRE</th>
			<th>Image</th>
			
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Books> listOfBooks = (List<Books>)obj;
	Iterator<Books> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Books book  = ii.next();
		%>
		<tr>
			<td><%=book.getBid() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			<td><img src="<%=book.getImageurl()%>"height="200px" width="150px"></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>