<%@page import="java.util.*" %>
<%@page import="com.bean.Items"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="1">
	<tr>
			<th>ITEMId</th>
			<th>ITEMNAME</th>
			<th>ITEMTYPE</th>
			<th>ITEMPRICE</th>
			<th>DELETE</th>
			<th>UPDATE</th>
			
	</tr>
<%  
	Object res=session.getAttribute("res2");
    if(res!=null){
	out.println(res);
	}
    Object obj = session.getAttribute("items");
	List<Items> listOfItems = (List<Items>)obj;
	Iterator<Items> ii = listOfItems.iterator();
	while(ii.hasNext()){
		Items item  = ii.next();
		%>
		<tr>
			<td><%=item.getItemId() %></td>
			<td><%=item.getItemName()%></td>
			<td><%=item.getItemType() %></td>
			<td><%=item.getItemPrice() %></td>
			<td><a href="deleteItem?itemId=<%=item.getItemId()%>">Delete Record</a> </td>
			<td><a href="UpdateItem?itemId=<%=item.getItemId()%>">Update Record</a> </td>
		</tr>
		<% 
	}
%>
</table>
<br>
<a href="MenuStore">_To_Store_Item_Details</a><br>

<a href="logout">Logout</a>
</body>
</html>