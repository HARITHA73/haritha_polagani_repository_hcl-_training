create database mini_haritha;

use mini_haritha;

create table admin(email varchar(20) primary key,password varchar(20));

insert into admin values("Hari@gmail.com","123");

create table users(email varchar(20) primary key,password varchar(20));

create table items(itemId int primary key,itemName varchar(50),itemType varchar(50),itemPrice float);

create table userorder(sno int primary key auto_increment,itemId int ,itemName varchar(50),itemType varchar(50),itemPrice float,name varchar(20));